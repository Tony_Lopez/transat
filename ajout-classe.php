
<?php $titre = "Ajouter une classe"; ?>
<?php ob_start(); ?>

<article>
    <?php
    session_start();
    
    if (isset($_SESSION['logged_in']['login']) !== TRUE) {
        
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");
    }
    ?>
    <h1>Ajouter une classe</h1>
    <form method="POST" action="insert_classe.php">
        
    </form>
</article>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>