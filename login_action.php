<?php
session_start();
require 'bdd/bddconfig.php';

$paramOK = false;
if (isset($_POST["login"])) {
    $login = strtolower(htmlspecialchars($_POST["login"]));
    if (isset($_POST["password"])) {
        $password = htmlspecialchars($_POST["password"]);
        $paramOK = true;
    }
}


if ($paramOK == true) {
    
    try {
        $objBdd = new PDO("mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8", $bddlogin, $bddpass);

        $PDOlistlogins = $objBdd->prepare("SELECT * FROM login WHERE login = :login ");
        $PDOlistlogins->bindParam(':login', $login, PDO::PARAM_STR);
        $PDOlistlogins->execute();
        
        $row_user = $PDOlistlogins->fetch();
        if ($row_user != false) {
           
            if ($password== $row_user['password']) {
               
                $session_data = array(
                    'idUser' => $row_user['idUser'],
                    'login' => $row_user['login'],
                    'pseudo' => $row_user['pseudo'],
                );
                
                session_regenerate_id();
                
                $_SESSION['logged_in'] = $session_data;
                $PDOlistlogins->closeCursor();
                
                $serveur = $_SERVER['HTTP_HOST'];
                $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
                $page = 'index.php';
                header("Location: http://$serveur$chemin/$page");
            } else {
                
                session_destroy();
                die('Authentification incorrecte');
            }
        } else {
            
            session_destroy();
            die('Authentification incorrecte');
        }
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
} else {
    die('Vous devez fournir un login et un mot de passe');
}
