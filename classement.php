<?php
require("bdd/bddconfig.php");
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $recupClass = $objBdd->query("SELECT * FROM `classebateau`");
} catch (Exception $prmE) {
    die("Erreur : " . $prmE->getMessage());
}

?>

<?php $titre = "Ajouter une classe"; ?>
<?php ob_start(); ?>

<article>
    <ul>

        <?php
        while ($classe = $recupClass->fetch()) {
        ?>
        <li> <?php echo $classe["typeCoque"] ?> <a href="liste-bateaux.php?classe=<?php echo $classe['idClasse'] ?>"> <?php echo $classe["nomClasse"] ?></a></li>
        <?php
        }
        $recupClass->closeCursor(); ?>

    </ul>

</article>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>