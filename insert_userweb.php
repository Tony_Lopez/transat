<?php


session_start();


$pseudo = 'Tony';
$login = strtolower('Tony');
$password_clair = 'toto';
$fonction = 'admin';


$hash_password = password_hash($password_clair, PASSWORD_BCRYPT);


require 'bdd/bddconfig.php';
try {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $PDOinsertuserweb = $objBdd->prepare("INSERT INTO user (pseudo, login, password, fonction) VALUES ( :pseudo, :login, :password, :fonction)");
    $PDOinsertuserweb->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
    $PDOinsertuserweb->bindParam(':login', $login, PDO::PARAM_STR);
    $PDOinsertuserweb->bindParam(':password', $hash_password, PDO::PARAM_STR);
    $PDOinsertuserweb->bindParam(':fonction', $fonction, PDO::PARAM_STR);
    $PDOinsertuserweb->execute();
    //récupérer la valeur de l'ID du nouveau bassin créé
    echo $lastId = $objBdd->lastInsertId();
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
