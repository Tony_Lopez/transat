<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/transat.css">
</head>

<body>
    <div id="container">
        <div class="login">
            <div class="titre">
                <h1>Transat</h1>
            </div>
        </div>
        <div class="connexion">
            <?php
            if (isset($_SESSION['logged_in']['login']) == TRUE) {

                echo 'Salut à toi, ' . $_SESSION['logged_in']['pseudo'] . '! ';

            ?>
                <br><br><a href="logout.php">Se déconnecter</a>
            <?php
            } else {

            ?>
                <a href="login.php">Connexion</a>
            <?php
            }
            ?>
        </div>
        <div class="themeDoc">
            <div class="theme">
                <h2> </h2>
                <ul>

                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="classement.php">Classement</a></li>

                </ul>
            </div>

            <div class="doc">
                <?php echo $contenu ?>
            </div>
        </div>
    </div>
</body>

</html>