<?php $titre = "Classe"; ?>
<?php session_start(); ?>
<?php
require "bdd/bddconfig.php";
$classe = $_GET['classe'];
try {
    $objBdd = new PDO(
        "mysql:host=$bddserver;
   dbname=$bddname;
   charset=utf8",
        $bddlogin,
        $bddpass
    );
    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    $listeBateau = $objBdd->query("SELECT * FROM bateau, classebateau WHERE bateau.idClasse = $classe GROUP BY bateau.nomBateau ORDER BY bateau.classementFinal ASC");
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}
?>
<?php ob_start(); ?>

<article>
    <h1>Classe</h1>
    <ul>
        <?php

        while ($bateau = $listeBateau->fetch()) {

            if ($bateau["classementFinal"] == 9999) {
                $bateauClassement = "AB";
            } else {
                $bateauClassement = $bateau["classementFinal"];
            }

        ?>

            <li> <?php echo $bateauClassement ?> <a href="detail-bateau.php?bateaux=<?php echo $bateau['idBateau'] ?>"> <?php echo $bateau["nomBateau"] ?></a></li>

        <?php
        }
        $listeBateau->closeCursor(); ?>
    </ul>

</article>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php'; ?>